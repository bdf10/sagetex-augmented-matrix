from sage.all import matrix, SR, var, vector
from itertools import izip


class SystemLatex:
    def __init__(self, A, b=None, varname='x'):
        if b is None:
            self.b = A.columns()[-1]
            self.A = matrix.column(A.columns()[:-1])
        else:
            self.A = A
            self.b = b
        self.A = self.A.change_ring(SR)
        self.varname = varname
        self.x = vector([var('{}_{}'.format(self.varname, i)) for i in range(1, A.ncols()+1)])
        self.nvars = A.ncols()

    def rref(self):
        R = self.A.augment(self.b).rref()
        return SystemLatex(R)

    def _latex_(self):
        tex = r'\begin{array}{' + 'rc'*self.nvars + 'r}\n'
        for vi, bi in zip(self.A, self.b):
            g = izip(vi, self.x)
            cij, xj = next(g)
            while cij == 0:
                try:
                    cij, xj = next(g)
                    tex += ' && '
                except StopIteration:
                    break
            else:
                tex += (cij * xj)._latex_()
            for cij, xj in g:
                if cij == 0:
                    tex += ' && '
                elif cij < 0:
                    tex += ' &-& ' + (abs(cij) * xj)._latex_()
                else:
                    tex += ' &+& ' + (cij * xj)._latex_()
            tex += ' &=& {}'.format(bi._latex_())
            tex += r'\\'
            tex += '\n'
        tex += r'\end{array}'
        return tex
