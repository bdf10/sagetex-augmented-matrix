(TeX-add-style-hook
 "example"
 (lambda ()
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "fitzmath"))
 :latex)

