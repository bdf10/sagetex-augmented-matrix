from sage.all import matrix


class AugmentedLatex:

    def __init__(self, A, b=None, varname='x'):
        self.varname = varname
        if b is None:
            b = A.columns()[-1]
            self.A = matrix.column(A.columns()[:-1])
        else:
            self.A = A
            self.b = b
        self.nvars = self.A.ncols()
        self.M = self.A.augment(b, subdivide=True)

    def rref(self):
        R = self.M.rref()
        b = R.columns()[-1]
        A = matrix.column(R.columns()[:-1])
        return AugmentedLatex(A, b)

    def _latex_(self):
        head = r'\begin{blockarray}{*{' + str(self.nvars+1) + '}r}\n'
        head += r'\begin{block}{*{' + str(self.nvars+1) + '}c}\n'
        for i in xrange(self.nvars):
            head += '{}_{{{}}} &'.format(self.varname, i+1)
        head += r'\\' + '\n'
        head += r'\end{block}' + '\n'
        head += r'\begin{block}{[' + 'r'*self.nvars + '|r]}\n'
        tex = self.M._latex_().split('\n')[1:-1]
        tex[-1] += r'\\'
        tail = '\n' + r'\end{block}' + '\n'
        tail += r'\end{blockarray}'
        return head + '\n'.join(tex) + tail
